# Integration Between Terraform and Puppet

This guide outlines the steps to set up and run the integration between Terraform and Puppet. By following these instructions, you'll be able to provision infrastructure using Terraform and configure it using Puppet.

## Prerequisites

Before proceeding, ensure you have the following prerequisites:

- **AWS Account:** You need an AWS account to launch EC2 instances.
- **Terraform Installed:** Ensure Terraform is installed on your local machine. You can download it from the [Terraform website](https://www.terraform.io/downloads.html).
- **SSH Key Pair:** Have an SSH key pair (e.g., forGitLab.pem) to connect to the EC2 instance.

## Installation Steps

### Install Terraform

1. Download and install Terraform on your local machine.
2. Add the Terraform executable to your system's PATH.

### Set Up AWS Credentials

1. Configure your AWS credentials using the AWS CLI or environment variables. Ensure your IAM user has the necessary permissions to create EC2 instances, VPCs, subnets, and security groups.

### Prepare Puppet Master EC2 Instance

1. Launch an EC2 instance using the provided Terraform script. This instance will serve as the Puppet Master.
2. Save the private key (forGitLab.pem) in a secure location.

### Configure Puppet

1. SSH into the Puppet Master EC2 instance using the private key.
2. The terraform script also includes provisioner blocks to install Puppet Server and configure it. Adjust these commands as needed.

## Configuration Details

### Terraform Script

- Use the provided Terraform script to launch the Puppet Master EC2 instance.
- Update the script with your specific details such as region, VPC ID, subnet ID, key name, and security group ID.
- The script also includes provisioner blocks to install Puppet Server and configure it. Adjust these commands as needed.

### SSH Connection

- Terraform uses SSH to connect to the EC2 instance. Ensure the SSH key pair (forGitLab.pem) is accessible and has appropriate permissions.
- Update the file path to the private key in the Terraform script if needed.

## Running the Integration

1. **Initialize Terraform:**
   In the directory containing your Terraform configuration files, run `terraform init` to initialize Terraform and download the necessary providers.

2. **Plan and Apply:**
   - Run `terraform plan` to preview the changes Terraform will make.
   - If everything looks correct, run `terraform apply` to apply the changes and launch the Puppet Master EC2 instance.
   
3. **Configure Puppet agents on other nodes to connect to the Puppet Master and manage them centrally.**

## Additional Considerations

- **Security:** Ensure your EC2 instance and Puppet Server are properly secured, and only necessary ports are open.
- **Monitoring and Maintenance:** Monitor the Puppet infrastructure for any issues and perform regular maintenance tasks as needed.

By following these steps and configurations, you can set up and run the integration between Terraform and Puppet successfully. Make sure to review and adjust the configurations according to your specific requirements and environment.


# Puppet Module: Apache

This Puppet module installs and configures Apache on the provisioned infrastructure component.

## Prerequisites
- Puppet installed on the Puppet Master EC2 instance.
- Access to the Puppet Master EC2 instance via SSH.
- Basic knowledge of Puppet manifests.

## Installation Steps
1. **Create Puppet Module:**
   Create a new Puppet module named `apache` inside the Puppet modules directory (`/etc/puppetlabs/code/environments/production/modules` by default).

2. **Define Manifests:**
   Inside the `apache` module directory, create the following manifest files:
   - **init.pp:** Defines the Apache class and includes other manifests.
   - **install.pp:** Installs the Apache package.
   - **config.pp:** Configures Apache settings.

3. **Write Puppet Manifests:**
   - **init.pp:**
     ```puppet
     class apache {
       include apache::install
       include apache::config
     }
     ```
   - **install.pp:**
     ```puppet
     class apache::install {
       package { 'apache2':
         ensure => installed,
       }
     }
     ```
   - **config.pp:**
     ```puppet
     class apache::config {
       file { '/etc/apache2/apache2.conf':
         ensure  => file,
         source  => 'puppet:///modules/apache/apache2.conf',
         require => Package['apache2'],
         notify  => Service['apache2'],
       }

       service { 'apache2':
         ensure => running,
         enable => true,
       }
     }
     ```

4. **Create Configuration Files:**
   Create any necessary configuration files for Apache, such as `apache2.conf`, and place them in the `files` directory within the `apache` module directory.

5. **Apply Puppet Manifests:**
   Include the `apache` class in the node's manifest file (usually located at `/etc/puppetlabs/code/environments/production/manifests/site.pp`).

   ```puppet
   node 'puppetMaster' {
     include apache
   }

